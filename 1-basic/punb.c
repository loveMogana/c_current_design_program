//
// Created by Administrator on 2018/12/9 0009.
//
#include <stdio.h>

int main(void) {
    int height, length, width, volume;
    height = 8;
    length = 12;
    width = 10;
    volume = height * length * width;
//    占位符 %d 用来指明在显示过程中变量height的值的显示位置.
//    \n 就是显示完值以后,跳到下一行.
    printf("Volumn:%d\n", volume);
//    值得注意的是 %d 只用于 int类型
//    如果要显示 float 类型,需要用 %f 来代替 %d
    float num;
    num = 1.1111111111111111111f;
//    %f 会显示出小数点后的6位数字.
    printf("Num: %f\n", num);
//    强制 %f 后的小数字后的个数
    printf("Num: %.2f\n", num);

}
